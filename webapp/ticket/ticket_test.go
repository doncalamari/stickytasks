package ticket

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"
)

func TestGetTickets(t *testing.T) {
	t.Log("Testing that GetTickets works.")
	{
		jira_config := getTestConfig(t)
		SetupTicketService(jira_config)

		ticket_ids := make([]Ticket_id, 2, 2)

		ticket_ids[0] = Ticket_id{}
		ticket_ids[0].Id = "ticket1" // change these to ticket ids that work for your institution
		ticket_ids[0].Print_description = true

		ticket_ids[1] = Ticket_id{}
		ticket_ids[1].Id = "ticket2"
		ticket_ids[1].Print_description = true

		tickets := GetTickets(ticket_ids)

		if tickets == nil || &tickets[0] == nil {
			fmt.Println("returned slice was empty")
			t.Fail()
		}

		for i := 0; i < 2; i++ {
			if tickets[i].Key == "" || tickets[i].Title == "" || tickets[i].Name == "" || tickets[i].Description == "" {
				fmt.Printf("index %d failed, %v\n", i, tickets[i])
				t.Fail()
			} else {
				fmt.Printf("ticket[%d]: %v\n", i, tickets[i])
			}
		}
	}
}

func getTestConfig(t *testing.T) JIRAConfig {
	file, _ := os.Open("config_test.json")
	decoder := json.NewDecoder(file)
	jira_config := JIRAConfig{}
	err := decoder.Decode(&jira_config)
	if err != nil {
		t.Log(fmt.Sprintf("Config file missing or corrupted. %v\n", err.Error()))
		t.Fail()
	} else {
		t.Log("Config succeeded.")
	}

	return jira_config
}
