package ticket

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"io/ioutil"

	"../jira"
)

const MAX_TICKETS int = 6

type Ticket struct {
	Key          string
	Title        string
	Name         string
	Description  string
	Story_points string
}

type Ticket_id struct {
	Id                string
	Print_description bool
}

type JIRAConfig struct {
	Host     string
	ApiPath  string
	UserName string
	Password string
}

var jira_config *JIRAConfig

func SetupTicketService(jira_config_arg JIRAConfig) {
	jira_config = &jira_config_arg
}

func GetTickets(ticket_ids []Ticket_id) []Ticket {

	if jira_config == nil {
		log.Printf("JIRA configuration not found. Exiting function.\n")
		return nil
	}

	tickets := make([]Ticket, 0, MAX_TICKETS)

	for i := 0; i < len(ticket_ids); i++ {
		if ticket_ids[i].Id == "" {
			continue
		}

		ticket := getTicket(ticket_ids[i])

		if ticket != nil {
			tickets = append(tickets, *ticket)
		}
	}

	return tickets
}

// I would have used a go package to do this work, but they are all pretty terrible.
func getTicket(ticket_id Ticket_id) *Ticket {
	client := http.DefaultClient
	url := fmt.Sprintf("%s%s/issue/%s", jira_config.Host, jira_config.ApiPath, ticket_id.Id)
	request, err := http.NewRequest("GET", url, nil)

	if err != nil {
		log.Printf("Error creating request for %s\n, %s\n", url, err)
		return nil
	}

	request.SetBasicAuth(jira_config.UserName, jira_config.Password)

	var response *http.Response
	response, err = client.Do(request)

	if err != nil {
		log.Printf("Error getting issue for %s. %s\n", url, err)
		return nil
	}

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Printf("Error opening body for issue. %s\n", err)
	}

	var issue jira.Issue
	err = json.Unmarshal(body, &issue)
	if err != nil {
		log.Printf("Error unmarshalling body for issues. body: %s,\n %s\n", body, err)
		return nil
	}

	ticket := Ticket{}

	if issue.Key == "" {
		return nil
	}
	log.Printf("ticket found: %s\n", issue.Key)
	ticket.Key = issue.Key
	ticket.Title = issue.Fields.Summary

	if ticket_id.Print_description {
		ticket.Description = issue.Fields.Description
	}

	if issue.Fields.Assignee.DisplayName == "" {
		ticket.Name = "(unassigned)"
	} else {
		ticket.Name = issue.Fields.Assignee.DisplayName
	}

	ticket.Story_points = fmt.Sprintf("%d", int(issue.Fields.Story_points))

	return &ticket

}
