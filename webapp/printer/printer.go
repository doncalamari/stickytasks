package printer

import (
	"io/ioutil"
	"log"
	"os"
	"os/exec"
)

type PrinterConfig struct {
	Printer_device  string // Printer device. Find the name using 'lpstat -s'
	Printer_command string
}

var printer_config *PrinterConfig

func SetupPrinterService(printer_config_arg PrinterConfig) {
	printer_config = &printer_config_arg
}

func SendBytesToPrinter(buffer []byte) {

	if printer_config == nil {
		log.Printf("Printer configuration not found. Exiting function.\n")
		return
	}

	file, err := ioutil.TempFile(os.TempDir(), "sticky")
	if err != nil {
		log.Printf("Error creating tmp file.\n", err)
	}

	n, err := file.Write(buffer)
	if err != nil {
		log.Printf("Error printing", err)
	}

	file.Close()
	log.Printf("Wrote temp %d bytes to %s\n", n, file.Name())
	log.Printf("Printing to %s.\n", printer_config.Printer_device)
	out, err := exec.Command(printer_config.Printer_command, "-d", printer_config.Printer_device, file.Name()).Output()
	if err != nil {
		log.Printf("Error executing command %s -d %s %s.\n %s\n", printer_config.Printer_command, printer_config.Printer_device, file.Name(), err)
	}

	log.Printf("Result of execution %s\n", out)
}
