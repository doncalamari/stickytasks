package jira

// created using http://www.jsonstruct.com/. I ain't typing all that shit...
type Issue struct {
	Expand string `json:"expand"`
	Fields struct {
		Assignee struct {
			DisplayName string `json:"displayName"`
		} `json:"assignee"`
		Description  string  `json:"description"`
		Summary      string  `json:"summary"`
		Story_points float64 `json:"customfield_10000"`
	} `json:"fields"`
	Key string `json:"key"`
}
