package main

import (
	"encoding/json"
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"time"

	"./handler"
	"./pdf"
	"./printer"
	"./slack"
	"./ticket"
)

type Configuration struct {
	Handler_config handler.HandlerConfig
	Jira_config    ticket.JIRAConfig
	Slack_config   slack.SlackConfig
	PDF_config     pdf.PDFConfig
	Printer_config printer.PrinterConfig
}

const (
	THREAD_CLEANUP_WAIT_TIME time.Duration = time.Duration(1) * time.Second
)

var (
	config_file = flag.String("config", "", "Location of configuration file.")
)

func main() {

	flag.Parse()
	if *config_file == "" {
		flag.Usage()
		log.Fatal("Config file location must be provided.")
	}

	file, _ := os.Open(*config_file)
	decoder := json.NewDecoder(file)
	configuration := Configuration{}
	err := decoder.Decode(&configuration)
	if err != nil {
		log.Fatal("Config file missing or corrupted.", err.Error())
	} else {
		log.Println("Config succeeded. Setting up handlers.")
	}

	pdf.SetupPdf(configuration.PDF_config)
	ticket.SetupTicketService(configuration.Jira_config)
	printer.SetupPrinterService(configuration.Printer_config)

	signals := make(chan os.Signal, 1)
	stop := make(chan bool, 1)
	stop <- false
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	if &configuration.Handler_config != nil {
		handler.SetupHandlers(configuration.Handler_config)
		go handler.ListenAndServe()
	}

	if &configuration.Slack_config != nil {
		slack.SetupSlack(configuration.Slack_config, configuration.Handler_config.Address)
		go slack.MonitorSlackServer(stop)
	}

	for {
		log.Println("Sticky Server started.")
		signal := <-signals
		stop <- true

		log.Printf("Signal received: ***%v***. Waiting %v for goroutines to die.\n", signal, THREAD_CLEANUP_WAIT_TIME)
		time.Sleep(THREAD_CLEANUP_WAIT_TIME)
		log.Printf("Shutting down.\n")
		break
	}

}
