package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"text/template"

	"../pdf"
	"../printer"
	"../ticket"
)

type HandlerConfig struct {
	Address       string // HTTP address and port to listen on
	Html_location string // Location of html files
	Log_location  string // Location of log files
}

var index_template *template.Template
var handler_config *HandlerConfig

func SetupHandlers(handler_config_arg HandlerConfig) {
	handler_config = &handler_config_arg
	http.Handle("/", http.StripPrefix("/", http.FileServer(http.Dir(handler_config.Html_location))))
	http.HandleFunc("/printBlankTemplate", PrintBlankTemplate)
	http.HandleFunc("/printFreeFormText", PrintFreeFormText)
	http.HandleFunc("/printTicket", PrintTicket)

	log.Printf("Handlers created. Will listen on address and port: %s\n", handler_config.Address)
}

func ListenAndServe() {
	if handler_config == nil {
		log.Println("Handler config not found. Exiting function.")
		return
	}

	// there appears to be no graceful way to stop listening.
	if err := http.ListenAndServe(handler_config.Address, nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}

func PrintBlankTemplate(writer http.ResponseWriter, request *http.Request) {
	buffer := pdf.GetBlankTemplate()
	printer.SendBytesToPrinter(buffer)

	writer.Write([]byte(fmt.Sprintf("Successfully printed template.")))
}

func PrintFreeFormText(writer http.ResponseWriter, request *http.Request) {
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		log.Printf("Error reading body %sn", err)
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	var tickets []ticket.Ticket
	err = json.Unmarshal(body, &tickets)

	if err != nil {
		log.Printf("Eror printing free form text. %s\n", err)
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	buffer := pdf.GetTicketPDF(tickets, false)
	printer.SendBytesToPrinter(buffer)

	writer.Write([]byte(fmt.Sprintf("Successfully printed free form text.")))
}

func PrintTicket(writer http.ResponseWriter, request *http.Request) {

	defer request.Body.Close()

	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		log.Printf("Error reading body for ticket_ids %s\n", err)
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	var ticket_ids []ticket.Ticket_id
	err = json.Unmarshal(body, &ticket_ids)

	if err != nil {
		log.Printf("Error unmarshalling body for ticket_ids %s,\n err: %s\n", body, err)
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Printf("ticket_ids: %v\n", ticket_ids)
	tickets := ticket.GetTickets(ticket_ids)

	if len(tickets) == 0 {
		log.Printf("No tickets found for any of %s\n", ticket_ids)
		http.Error(writer, "No tickets found.", http.StatusInternalServerError)
		return
	}

	buffer := pdf.GetTicketPDF(tickets, false)
	printer.SendBytesToPrinter(buffer)

	writer.Write([]byte(fmt.Sprintf("Successfully printed %d tickets.", len(tickets))))
}
