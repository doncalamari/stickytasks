package slack

import (
	"fmt"
	"log"
	"strings"

	"../ticket"

	"github.com/nlopes/slack"
)

type SlackConfig struct {
	Api_key  string
	Bot_name string
}

func (s SlackConfig) String() string {
	return fmt.Sprintf("{\n  Api_key: *****%s,\n Bot_name: %s\n}\n", s.Api_key[len(s.Api_key)-4:], s.Bot_name)
}

var (
	slack_config    *SlackConfig
	client          *slack.Client
	bot_commands    map[string]*BotCommand
	print_queue     map[string][]ticket.Ticket_id
	web_server_port string
)

func init() {
	bot_commands = createBotCommands()
	print_queue = make(map[string][]ticket.Ticket_id)
	log.Printf("Slack initialized: %v\n", slack_config)
}

func SetupSlack(slack_config_arg SlackConfig, web_server_port_arg string) {
	slack_config = &slack_config_arg
	web_server_port = web_server_port_arg

	client = slack.New(slack_config.Api_key)
	client.SetDebug(false)

	log.Printf("Slack initialized: %v\n", slack_config)
}

func MonitorSlackServer(stop chan bool) {

	if slack_config == nil {
		log.Printf("Slack configuration not found. Exiting function.\n")
		return
	}

	rtm := client.NewRTM()
	go rtm.ManageConnection()
	log.Printf("Slack RTM client started.\n")

	defer func() {
		rtm.Disconnect()

		if r := recover(); r != nil {
			log.Printf("Monitor function has crashed. %v", r)
		} else {
			log.Printf("Monitor function closed normally.")
		}
	}()

	for {
		select {
		case msg := <-rtm.IncomingEvents:

			switch event := msg.Data.(type) {

			case *slack.MessageEvent:

				if isDMChannel(event.Msg.Channel) && event.User != "" {
					for command_key, command := range bot_commands {
						if strings.HasPrefix(event.Msg.Text, command_key) {
							command.Func(event)
						}
					}
				}

			case *slack.RTMError:
				log.Printf("Error: %s\n", event.Error())

			}
		default:
			select {
			case die := <-stop:
				if die {
					log.Println("Stop message received.")
					break
				}

			default:
				// do nothing
			}
		}

	}

	log.Printf("Slack RTM client stopped.\n")
}

func isDMChannel(channel_name string) bool {
	return channel_name[0] == 'D'
}

func postMessage(channel string, message string) {
	params := slack.PostMessageParameters{
		Username: slack_config.Bot_name,
	}
	_, _, err := client.PostMessage(channel, message, params)
	if err != nil {
		log.Printf("Error posting message '%s'. Error: %v\n", message, err)
		return
	}
}

func logFunctionMessage(function_name string, user_id string) {
	log.Printf("User %s asked for the '%s' function.\n", user_id, function_name)
}
