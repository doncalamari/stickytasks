package slack

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"

	"../pdf"
	"../printer"
	"../ticket"
)

type TestConfiguration struct {
	Jira_config    ticket.JIRAConfig
	Slack_config   SlackConfig
	PDF_config     pdf.PDFConfig
	Printer_config printer.PrinterConfig
}

func TestMonitorChannel(t *testing.T) {
	t.Log("Testing that MonitorChannel works.")
	{
		test_config := getTestConfig(t)

		pdf.SetupPdf(test_config.PDF_config)
		ticket.SetupTicketService(test_config.Jira_config)
		printer.SetupPrinterService(test_config.Printer_config)
		SetupSlack(test_config.Slack_config, ":8080")

		MonitorChannel()
	}
}

func getTestConfig(t *testing.T) TestConfiguration {
	file, _ := os.Open("config_test.json")
	decoder := json.NewDecoder(file)
	test_config := TestConfiguration{}
	err := decoder.Decode(&test_config)
	if err != nil {
		t.Log(fmt.Sprintf("Config file missing or corrupted. %v\n", err.Error()))
		t.Fail()
	} else {
		t.Log("Config succeeded.")
	}

	return test_config
}
