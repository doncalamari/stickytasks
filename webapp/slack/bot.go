package slack

import (
	"bytes"
	"fmt"
	"log"
	"net"
	"strings"

	"../pdf"
	"../printer"
	"../ticket"

	"github.com/nlopes/slack"
)

const (
	HELP_COMMAND     string = "help"
	PRINT_COMMAND    string = "print"
	TICKETS_COMMAND  string = "tickets"
	STATS_COMMAND    string = "stats"
	ADD_COMMAND      string = "add"
	WEB_COMMAND      string = "web"
	REMOVE_COMMAND   string = "remove"
	TEMPLATE_COMMAND string = "template"
)

type BotFunction func(message_event *slack.MessageEvent)

type BotCommand struct {
	Description string
	Usage       string
	Hidden      bool
	Func        BotFunction
}

func createBotCommands() map[string]*BotCommand {
	bot_commands := make(map[string]*BotCommand)

	bot_commands[HELP_COMMAND] = &BotCommand{
		Description: "Shows help screen.",
		Usage:       HELP_COMMAND,
		Func:        helpBotFunc,
	}

	bot_commands[PRINT_COMMAND] = &BotCommand{
		Description: "Sends the tickets in your list to the printer. More than 6 tickets will print on multiple pages, so fill the printer with as many filled templates as you need.",
		Usage:       PRINT_COMMAND,
		Func:        printBotFunc,
	}

	bot_commands[TICKETS_COMMAND] = &BotCommand{
		Description: "Shows you what tickets are in your list.",
		Usage:       TICKETS_COMMAND,
		Func:        ticketsBotFunc,
	}

	bot_commands[STATS_COMMAND] = &BotCommand{
		Description: "Gives some stats on the program.",
		Hidden:      true,
		Func:        statsBotFunc,
	}

	bot_commands[ADD_COMMAND] = &BotCommand{
		Description: "Adds a ticket or collection of tickets to a user's list.",
		Usage:       fmt.Sprintf("%s <ticket id or space separated list of ticket ids> ", ADD_COMMAND),
		Func:        addBotFunc,
	}

	bot_commands[REMOVE_COMMAND] = &BotCommand{
		Description: "Removes a ticket or list of tickets to a user's list.",
		Usage:       fmt.Sprintf("%s <ticket id or space separated list of ticket ids> ", REMOVE_COMMAND),
		Func:        removeBotFunc,
	}

	bot_commands[WEB_COMMAND] = &BotCommand{
		Description: "Displays the link to and information of the sticky printer web application.",
		Usage:       fmt.Sprintf("%s", WEB_COMMAND),
		Func:        webBotFunc,
	}

	bot_commands[TEMPLATE_COMMAND] = &BotCommand{
		Description: "Prints a copy of the blank template.",
		Usage:       fmt.Sprintf("%s", TEMPLATE_COMMAND),
		Func:        templateBotFunc,
	}

	return bot_commands
}

func helpBotFunc(message_event *slack.MessageEvent) {
	logFunctionMessage(HELP_COMMAND, message_event.User)

	var buffer bytes.Buffer
	buffer.WriteString("If you are just starting out, you need to print a blank template on plain paper first. ")
	buffer.WriteString("Then afix the sticky-notes where the boxes indicate. ")
	buffer.WriteString("Load the filled template into the printer and use the commands shown below to load your list with tickets and print.\n")

	for key, value := range bot_commands {
		if !value.Hidden {
			buffer.WriteString("*Command:* ")
			buffer.WriteString(key)
			buffer.WriteString("\n")
			buffer.WriteString("*Description:* ")
			buffer.WriteString(value.Description)
			buffer.WriteString("\n")
			buffer.WriteString("*Usage:* \n>")
			buffer.WriteString(value.Usage)
			buffer.WriteString("\n\n")
		}
	}

	postMessage(message_event.Channel, buffer.String())
}

func printBotFunc(message_event *slack.MessageEvent) {
	logFunctionMessage(PRINT_COMMAND, message_event.User)

	if len(print_queue[message_event.User]) == 0 {
		postMessage(message_event.Channel, "No tickets in your list.\n")
		return
	}

	tickets := ticket.GetTickets(print_queue[message_event.User])

	if len(tickets) == 0 {
		postMessage(message_event.Channel, fmt.Sprintf("No tickets found for any of %s\n", print_queue[message_event.User]))
		return
	}

	// can only print 6 tickets at a time
	for i := 0; i < len(tickets); i = i + ticket.MAX_TICKETS {
		var buffer []byte
		if 6 > len(tickets[i:]) {
			buffer = pdf.GetTicketPDF(tickets[i:], false)
		} else {
			buffer = pdf.GetTicketPDF(tickets[i:i+ticket.MAX_TICKETS], false)
		}

		printer.SendBytesToPrinter(buffer)
	}

	var removed_ticket_ids []string

	for _, ticket := range tickets {
		deleted := 0

		for i := range print_queue[message_event.User] {
			j := i - deleted

			if print_queue[message_event.User][j].Id == ticket.Key {
				print_queue[message_event.User] = print_queue[message_event.User][:j+copy(print_queue[message_event.User][j:], print_queue[message_event.User][j+1:])]
				removed_ticket_ids = append(removed_ticket_ids, ticket.Key)
				deleted++
			}
		}
	}

	postMessage(message_event.Channel, fmt.Sprintf(">Printed tickets %s and removed them from your list.", removed_ticket_ids))

	if len(print_queue[message_event.User]) > 0 {
		var remaining_ticket_ids []string
		for _, t := range print_queue[message_event.User] {
			remaining_ticket_ids = append(remaining_ticket_ids, t.Id)
		}

		postMessage(message_event.Channel, fmt.Sprintf("%s were not found and are still in your list.", remaining_ticket_ids))
	}
}

func ticketsBotFunc(message_event *slack.MessageEvent) {
	logFunctionMessage(TICKETS_COMMAND, message_event.User)

	requested_ticket_ids := make([]string, len(print_queue[message_event.User]), len(print_queue[message_event.User]))

	for i := range print_queue[message_event.User] {
		requested_ticket_ids[i] = print_queue[message_event.User][i].Id
	}

	postMessage(message_event.Channel, fmt.Sprintf("%v are the ticket ids in your list.", requested_ticket_ids))
}

func statsBotFunc(message_event *slack.MessageEvent) {
	// TODO
	logFunctionMessage(STATS_COMMAND, message_event.User)
}

func addBotFunc(message_event *slack.MessageEvent) {
	logFunctionMessage(ADD_COMMAND, message_event.User)

	if len(message_event.Msg.Text) <= len(ADD_COMMAND)+1 {
		postMessage(message_event.Channel, "Please enter some ticket ids to add to your list.")
		return
	}

	if print_queue[message_event.User] == nil {
		log.Printf("Recreating print list.")
		print_queue[message_event.User] = make([]ticket.Ticket_id, 0, 0)
	}

	requested_ticket_ids := strings.Split(message_event.Msg.Text[len(ADD_COMMAND)+1:], " ")
	new_ticket_ids := make([]ticket.Ticket_id, len(requested_ticket_ids), len(requested_ticket_ids))

	for i := range requested_ticket_ids {
		new_ticket_ids[i] = ticket.Ticket_id{
			Id:                requested_ticket_ids[i],
			Print_description: false,
		}
	}

	print_queue[message_event.User] = append(print_queue[message_event.User], new_ticket_ids...)

	postMessage(message_event.Channel, fmt.Sprintf(">Added %v to your list.", requested_ticket_ids))
}

func removeBotFunc(message_event *slack.MessageEvent) {
	logFunctionMessage(REMOVE_COMMAND, message_event.User)

	if len(message_event.Msg.Text) <= len(REMOVE_COMMAND)+1 {
		postMessage(message_event.Channel, "Please enter some ticket ids to remove from your list.")
		return
	}

	if print_queue[message_event.User] == nil {
		postMessage(message_event.Channel, "You have no ticket ids to remove in your list.")
		return
	}

	requested_ticket_ids := strings.Split(message_event.Msg.Text[len(REMOVE_COMMAND)+1:], " ")
	for i := range requested_ticket_ids {
		remove_index := -1
		for j := range print_queue[message_event.User] {
			if print_queue[message_event.User][j].Id == requested_ticket_ids[i] {
				remove_index = j
				break
			}
		}

		if remove_index >= 0 {
			print_queue[message_event.User] = append(print_queue[message_event.User][:remove_index], print_queue[message_event.User][remove_index+1:]...)
		} else {
			postMessage(message_event.Channel, fmt.Sprintf("The ticked id %s was not in the list.", requested_ticket_ids[i]))
		}
	}

}

func webBotFunc(message_event *slack.MessageEvent) {
	logFunctionMessage(WEB_COMMAND, message_event.User)

	addrs, err := net.InterfaceAddrs()
	// handle err
	if err != nil {
		log.Printf("Error getting interfaces. %v\n", err)
		return
	}

	for _, addr := range addrs {
		var ip net.IP
		switch v := addr.(type) {
		case *net.IPNet:
			ip = v.IP
			if ip.To4() != nil { // filter out any IPv6 addresses
				if !strings.Contains(ip.String(), "127.0.0.1") { // remove loopback
					postMessage(message_event.Channel, fmt.Sprintf("Web server here: http://%s%s", ip, web_server_port))
				}
			}

		default:
			// skip everything else
		}
	}
}

func templateBotFunc(message_event *slack.MessageEvent) {
	logFunctionMessage(TEMPLATE_COMMAND, message_event.User)

	buffer := pdf.GetBlankTemplate()
	printer.SendBytesToPrinter(buffer)

	postMessage(message_event.Channel, ">Printed a copy of the template.")
}
