package pdf

import (
	"log"
	"strconv"
	"strings"

	"../ticket"
	"../wordwrap"
	"github.com/signintech/gopdf"
)

const (
	PAGE_WIDTH_PT            float64 = 612
	PAGE_HEIGHT_PT           float64 = 792
	COLUMNS                  int     = 2
	ROWS                     int     = 3
	UPPER_LEFT_X_PT          float64 = 54
	UPPER_LEFT_Y_PT          float64 = 36
	POST_IT_WIDTH_PT         float64 = 216
	HORIZONTAL_SPACER_PT     float64 = 72
	VERTICAL_SPACER_PT       float64 = 36
	TEMPLATE_NUM_X_OFFSET_PT float64 = 54
	TEMPLATE_NUM_Y_OFFSET_PT float64 = 36
	TEXT_X_OFFSET_PT         float64 = 10
	TEXT_Y_OFFSET_PT         float64 = 10
	font_id                  string  = "somefont"
)

type PDFConfig struct {
	Font_location          string // Location of font file
	Template_font_size     int
	Key_font_size          int
	Title_font_size        int
	Name_font_size         int
	Description_font_size  int
	Story_points_font_size int
}

var pdf_config *PDFConfig

func SetupPdf(pdf_config_arg PDFConfig) {
	pdf_config = &pdf_config_arg
}

func GetBlankTemplate() []byte {

	if pdf_config == nil {
		log.Printf("PDF configuration not found. Exiting function.\n")
		return nil
	}

	pdf := createBasicPDF()

	pdf.SetX(UPPER_LEFT_X_PT / 2.0)
	pdf.SetY(UPPER_LEFT_Y_PT / 2.0)
	pdf.SetTextColor(20, 20, 20)
	err := pdf.SetFont(font_id, "", pdf_config.Description_font_size)
	if err != nil {
		log.Printf("Error printing template. %s\n", err)
		return nil
	}

	pdf.Cell(nil, "Load into printer with this side up and with the arrow pointing into the printer.")

	pdf.SetLineWidth(1)
	pdf.SetLineType("solid")

	// draw arrow
	pdf.Line(PAGE_WIDTH_PT/2.0, UPPER_LEFT_Y_PT*2.0, PAGE_WIDTH_PT/2.0, POST_IT_WIDTH_PT/2.0+UPPER_LEFT_Y_PT*2.0)
	pdf.Line(PAGE_WIDTH_PT/2.0, UPPER_LEFT_Y_PT*2.0, PAGE_WIDTH_PT/2.0-20, UPPER_LEFT_Y_PT*2.0+30)
	pdf.Line(PAGE_WIDTH_PT/2.0, UPPER_LEFT_Y_PT*2.0, PAGE_WIDTH_PT/2.0+20, UPPER_LEFT_Y_PT*2.0+30)

	pdf.SetTextColor(220, 220, 220)
	err = pdf.SetFont(font_id, "", pdf_config.Template_font_size)
	if err != nil {
		log.Printf("Error printing template. %s\n", err)
		return nil
	}

	var counter int64 = 0
	for row := 0; row < ROWS; row++ {
		for column := 0; column < COLUMNS; column++ {

			x1, y1, x2, y2 := getPostItSquare(column, row)
			pdf.Line(x1, y1, x2, y1) // top line
			pdf.Line(x1, y2, x2, y2) // bottom line
			pdf.Line(x1, y1, x1, y2) // left line
			pdf.Line(x2, y1, x2, y2) // right line

			pdf.SetX(x1 + TEMPLATE_NUM_X_OFFSET_PT)
			pdf.SetY(y1 + TEMPLATE_NUM_Y_OFFSET_PT)
			counter += 1
			pdf.Cell(nil, strconv.FormatInt(counter, 10)) // draw template number
		}
	}

	return pdf.GetBytesPdf()
}

func GetTicketPDF(tickets []ticket.Ticket, debug_layout bool) []byte {
	pdf := createBasicPDF()

	pdf.SetTextColor(0, 0, 0)

	for i := 0; i < len(tickets); i++ {
		var x1, y1, x2, y2 float64
		x1, y1, x2, y2 = getPostItSquare(i%COLUMNS, i/COLUMNS)

		if debug_layout {
			pdf.SetLineWidth(1)
			pdf.SetLineType("solid")

			pdf.Line(x1, y1, x2, y1) // top line
			pdf.Line(x1, y2, x2, y2) // bottom line
			pdf.Line(x1, y1, x1, y2) // left line
			pdf.Line(x2, y1, x2, y2) // right line
		}

		// print the key
		pdf.SetX(x1 + TEXT_X_OFFSET_PT)
		pdf.SetY(y1 + TEXT_Y_OFFSET_PT)
		pdf.SetFont(font_id, "", pdf_config.Key_font_size)
		pdf.Cell(nil, tickets[i].Key)
		pdf.Br(float64(TEXT_Y_OFFSET_PT))

		// print the title
		pdf.SetFont(font_id, "", pdf_config.Title_font_size)
		pdf.SetX(x1 + TEXT_X_OFFSET_PT)
		putWrappedText(tickets[i].Title, float64(pdf_config.Title_font_size), pdf, x1, x2, y2)
		pdf.Br(float64(TEXT_Y_OFFSET_PT))
		pdf.Br(float64(TEXT_Y_OFFSET_PT))

		// print the assigned name
		pdf.SetX(x1 + TEXT_X_OFFSET_PT)
		pdf.SetFont(font_id, "", pdf_config.Name_font_size)
		pdf.Cell(nil, tickets[i].Name)
		pdf.Br(float64(TEXT_Y_OFFSET_PT))

		// print the description
		pdf.SetX(x1 + TEXT_X_OFFSET_PT)
		pdf.SetFont(font_id, "", pdf_config.Description_font_size)
		putWrappedText(tickets[i].Description, float64(pdf_config.Description_font_size), pdf, x1, x2, y2)

		// print the story points
		if tickets[i].Story_points != "" && tickets[i].Story_points != "0" {
			pdf.SetFont(font_id, "", pdf_config.Story_points_font_size)
			pdf.Curr.Font_ISubset.AddChars(tickets[i].Story_points)
			points_width, _ := pdf.MeasureTextWidth(tickets[i].Story_points)
			pdf.SetX(x2 - points_width - TEXT_X_OFFSET_PT)
			pdf.SetY(y1 + TEXT_Y_OFFSET_PT)
			pdf.Cell(nil, tickets[i].Story_points)
			pdf.SetLineWidth(1)
			pdf.Oval(
				x2-points_width-TEXT_X_OFFSET_PT-3,
				y1+TEXT_Y_OFFSET_PT-3,
				x2-TEXT_X_OFFSET_PT+3,
				y1+TEXT_Y_OFFSET_PT+float64(pdf_config.Story_points_font_size))
		}

	}

	return pdf.GetBytesPdf()
}

func putWrappedText(long_text string, font_size float64, pdf *gopdf.GoPdf, x1 float64, x2 float64, y2 float64) {
	formatted_text := strings.Replace(long_text, "\r", "", -1) // these things really screw with the PDF lib
	// add the text or the call to measure fails
	pdf.Curr.Font_ISubset.AddChars(formatted_text)
	total_text_width, _ := pdf.MeasureTextWidth(formatted_text)

	average_char_width := total_text_width / float64(len(formatted_text))

	max_characters_per_line := (x2 - x1 - (TEXT_X_OFFSET_PT * 2)) / average_char_width
	list := wordwrap.WrapString(formatted_text, uint(max_characters_per_line))

	for i := 0; i < len(list); i++ {
		pdf.SetX(x1 + TEXT_X_OFFSET_PT)
		pdf.SetY(pdf.GetY() + font_size)

		if pdf.GetY()+font_size > y2 { // cuts off too many lines
			break
		}
		pdf.Cell(nil, list[i])
	}
}
func getPostItSquare(column int, row int) (float64, float64, float64, float64) {
	x1 := UPPER_LEFT_X_PT + (float64(column) * (HORIZONTAL_SPACER_PT + POST_IT_WIDTH_PT))
	y1 := UPPER_LEFT_Y_PT + (float64(row) * VERTICAL_SPACER_PT) + (POST_IT_WIDTH_PT * float64(row))
	x2 := x1 + POST_IT_WIDTH_PT
	y2 := y1 + POST_IT_WIDTH_PT

	return x1, y1, x2, y2
}

func createBasicPDF() *gopdf.GoPdf {
	pdf := gopdf.GoPdf{}
	pdf.Start(gopdf.Config{Unit: "pt", PageSize: gopdf.Rect{W: PAGE_WIDTH_PT, H: PAGE_HEIGHT_PT}})
	pdf.AddPage()

	err := pdf.AddTTFFont(font_id, pdf_config.Font_location)
	if err != nil {
		log.Printf("Error adding font. %s\n", err)
		return nil
	}

	return &pdf
}
