package pdf

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"

	"../ticket"
)

func TestGetBlankTemplate(t *testing.T) {
	t.Log("Testing that CreateBlankTemplate works.")
	{

		SetupPdf(getTestConfig(t))
		buffer := GetBlankTemplate()

		if buffer == nil {
			t.Fail()
		}

		writeToFile("output_blank_test.pdf", buffer)
	}
}

func TestGetTicketPDF(t *testing.T) {
	t.Log("Testing that GetTicketPDF works.")
	{
		tickets := make([]ticket.Ticket, 6, 6)
		tickets[0] = ticket.Ticket{}

		tickets[0].Key = "YY-1234"
		tickets[0].Title = "This is title #1"
		tickets[0].Name = "Some Bozo1"
		tickets[0].Description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
		tickets[0].Story_points = "8"

		tickets[1] = ticket.Ticket{}
		tickets[1].Key = "YY-1235"
		tickets[1].Title = "This is title #2 asdfksdakfjas dkljfkldsj akjdfkjdsakfjdskjf"
		tickets[1].Name = "Some Bozo2"
		tickets[1].Description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n"
		tickets[1].Description = tickets[1].Description + "Phasellus mattis tincidunt nibh.\r\nCras orci urna, blandit id, pretium vel, aliquet ornare, felis. Maecenas scelerisque sem non nisl. Fusce sed lorem in enim dictum bibendum."
		tickets[1].Story_points = "18"

		tickets[2] = ticket.Ticket{}
		tickets[2].Key = "YY-1236"
		tickets[2].Title = "This is title #3"
		tickets[2].Name = "Some Bozo3"
		tickets[2].Description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
		tickets[2].Story_points = "0"

		tickets[3] = ticket.Ticket{}
		tickets[3].Key = "YY-1237"
		tickets[3].Title = "This is title #4"
		tickets[3].Name = "Some Bozo4"
		tickets[3].Description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
		tickets[3].Story_points = ""

		tickets[4] = ticket.Ticket{}
		tickets[4].Key = "YY-1238"
		tickets[4].Title = "This is title #5"
		tickets[4].Name = "Some Bozo5"
		tickets[4].Description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
		tickets[4].Story_points = "8"

		tickets[5] = ticket.Ticket{}
		tickets[5].Key = "YY-1239"
		tickets[5].Title = "This is title #6"
		tickets[5].Name = "Some Bozo6"
		tickets[5].Description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
		tickets[5].Story_points = "8"

		SetupPdf(getTestConfig(t))
		buffer := GetTicketPDF(tickets, true)

		if buffer == nil {
			t.Fail()
		}

		writeToFile("output_freeform_test.pdf", buffer)
	}
}

func writeToFile(filename string, buffer []byte) {

	file, err := os.Create(filename)

	if err != nil {
		fmt.Println(err)
	}

	n, err := file.Write(buffer)

	if err != nil {
		fmt.Println(n, err)
	}

	file.Close()
}

func getTestConfig(t *testing.T) PDFConfig {
	file, _ := os.Open("config_test.json")
	decoder := json.NewDecoder(file)
	pdf_config := PDFConfig{}
	err := decoder.Decode(&pdf_config)
	if err != nil {
		t.Log(fmt.Sprintf("Config file missing or corrupted. %v\n", err.Error()))
		t.Fail()
	} else {
		t.Log("Config succeeded.")
	}

	return pdf_config
}
