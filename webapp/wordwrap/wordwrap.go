package wordwrap

import (
	"bytes"
	"unicode"
)

func WrapString(s string, lim uint) []string {

	list := make([]string, 0)

	var current uint
	var word_buffer, space_buffer bytes.Buffer
	init := make([]byte, 0, len(s))
	buffer := bytes.NewBuffer(init)
	for _, char := range s {

		if char == '\n' {
			if word_buffer.Len() == 0 {
				if current+uint(space_buffer.Len()) > lim {
					current = 0
				} else {
					current += uint(space_buffer.Len())
					space_buffer.WriteTo(buffer)
				}
				space_buffer.Reset()
			} else {
				current += uint(space_buffer.Len() + word_buffer.Len())
				space_buffer.WriteTo(buffer)
				space_buffer.Reset()
				word_buffer.WriteTo(buffer)
				word_buffer.Reset()
			}
			list = append(list, buffer.String())
			buffer.Reset()
			current = 0
		} else if unicode.IsSpace(char) {
			if space_buffer.Len() == 0 || word_buffer.Len() > 0 {
				current += uint(space_buffer.Len() + word_buffer.Len())
				space_buffer.WriteTo(buffer)
				space_buffer.Reset()
				word_buffer.WriteTo(buffer)
				word_buffer.Reset()
			}

			space_buffer.WriteRune(char)
		} else {

			word_buffer.WriteRune(char)

			if current+uint(space_buffer.Len()+word_buffer.Len()) > lim && uint(word_buffer.Len()) < lim {
				list = append(list, buffer.String())
				current = 0
				space_buffer.Reset()
				buffer.Reset()
			}
		}
	}

	if word_buffer.Len() == 0 {
		if current+uint(space_buffer.Len()) <= lim {
			space_buffer.WriteTo(buffer)
		}
	} else {
		space_buffer.WriteTo(buffer)
		word_buffer.WriteTo(buffer)
	}

	list = append(list, buffer.String())
	return list
}
